package com.br.itau.leadCollector.services;

import com.br.itau.leadCollector.models.Lead;
import com.br.itau.leadCollector.models.Produto;
import com.br.itau.leadCollector.repositories.LeadRepository;
import com.br.itau.leadCollector.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class LeadCollectorService {

    @Autowired
    private LeadRepository leadRepository;

    @Autowired
    private ProdutoRepository produtoRepository;

    public Iterable<Produto> buscarTodosProdutos(List<Integer> produtosId){
        Iterable<Produto> produtosIterable = produtoRepository.findAllById(produtosId);
        return produtosIterable;
    }

    public Optional<Lead> buscarPorId(int id){
        Optional<Lead> leadOptional = leadRepository.findById(id);
        return leadOptional;
    }

    public Lead salvarLead(Lead lead) {
        Lead leadObjeto = leadRepository.save(lead);
        return leadObjeto;
    }

    public Iterable<Lead> buscarTodosLeads(){
        Iterable<Lead> leads = leadRepository.findAll();
        return leads;
    }

    public Lead atualizarLead(Lead lead){
        Optional<Lead> leadOptional = buscarPorId(lead.getId());
        if (leadOptional.isPresent()){
            Lead leadData = leadOptional.get();
            if(lead.getNome() == null){
                lead.setNome(leadData.getNome());
            }
            if(lead.getEmail() == null){
                lead.setEmail(leadData.getEmail());
            }
            if(lead.getTipoDeLead() == null){
                lead.setTipoDeLead(leadData.getTipoDeLead());
            }
        }
        Lead leadObjeto = leadRepository.save(lead);
        return leadObjeto;
    }

    public void deletarLead(Lead lead){
        leadRepository.delete(lead);
    }
}
