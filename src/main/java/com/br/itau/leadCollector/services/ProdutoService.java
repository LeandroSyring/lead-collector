package com.br.itau.leadCollector.services;

import com.br.itau.leadCollector.models.Produto;
import com.br.itau.leadCollector.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.spel.ast.NullLiteral;
import org.springframework.stereotype.Service;


import java.util.Optional;

@Service
public class ProdutoService {

    @Autowired
    private ProdutoRepository produtoRepository;

    public Iterable<Produto> buscarTodosProdutos(){
        return produtoRepository.findAll();
    }

    public Optional<Produto> buscarProdutoId(Integer id){
        Optional<Produto> optionalProduto = produtoRepository.findById(id);
        return optionalProduto;
    }

    public Produto salvarProduto(Produto produto){
        Produto objectProduto = produtoRepository.save(produto);
        return objectProduto;
    }

    public Produto atualizarProduto(Produto produto){

        Produto produtoOriginal = buscarProdutoId(produto.getId()).get();

        if(produto.getNome() == null){
            produto.setNome(produtoOriginal.getNome());
        }
        if(produto.getDescricao() == null){
            produto.setDescricao(produtoOriginal.getDescricao());
        }
        if(produto.getPreco() == null){
            produto.setPreco(produtoOriginal.getPreco());
        }

        return produtoRepository.save(produto);
    }

    public void deletarProduto(Produto produto){
        produtoRepository.delete(produto);
    }
}
