package com.br.itau.leadCollector.repositories;

import com.br.itau.leadCollector.models.Produto;
import org.springframework.data.repository.CrudRepository;

public interface ProdutoRepository extends CrudRepository<Produto, Integer> {
}
