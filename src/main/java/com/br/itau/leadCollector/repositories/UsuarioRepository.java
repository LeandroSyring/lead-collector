package com.br.itau.leadCollector.repositories;

import com.br.itau.leadCollector.models.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer> {

    Usuario findByEmail(String email);

}
