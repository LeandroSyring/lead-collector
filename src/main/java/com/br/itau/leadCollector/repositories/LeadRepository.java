package com.br.itau.leadCollector.repositories;

import com.br.itau.leadCollector.models.Lead;
import org.springframework.data.repository.CrudRepository;

public interface LeadRepository extends CrudRepository<Lead, Integer> {

}
