package com.br.itau.leadCollector.controllers;

import com.br.itau.leadCollector.models.Lead;
import com.br.itau.leadCollector.models.Produto;
import com.br.itau.leadCollector.services.LeadCollectorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/LeadsCollectors")
public class LeadCollectorController {

    @Autowired
    private LeadCollectorService leadCollectorService;

    @GetMapping
    public Iterable<Lead> buscarTodos(){
        return leadCollectorService.buscarTodosLeads();
    }

    @GetMapping("/{id}")
    public Lead buscarLead(@PathVariable Integer id){
        Optional<Lead> leadOptional = leadCollectorService.buscarPorId(id);

        if(leadOptional.isPresent()){
            return leadOptional.get();
        }else{
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping
    public ResponseEntity<Lead> inserirLead(@RequestBody @Valid Lead lead){
        List<Integer> produtosId = new ArrayList<>();
        for(Produto produto: lead.getProdutos()){
            produtosId.add(produto.getId());
        }
        Iterable<Produto> produtosIterable = leadCollectorService.buscarTodosProdutos(produtosId);

        lead.setProdutos((List)produtosIterable);

        Lead leadReturn = leadCollectorService.salvarLead(lead);
        return ResponseEntity.status(201).body(leadReturn);
    }

    @PutMapping("/{id}")
    public Lead atualizarLead(@PathVariable Integer id, @RequestBody Lead lead){
        lead.setId(id);
        Lead leadObjeto = leadCollectorService.atualizarLead(lead);
        return leadObjeto;
    }

    @DeleteMapping("/{id}")
    public Lead deletarLead(@PathVariable Integer id){
        Optional<Lead> leadOptional = leadCollectorService.buscarPorId(id);
        if(leadOptional.isPresent()){
            leadCollectorService.deletarLead(leadOptional.get());
            return leadOptional.get();
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }
}