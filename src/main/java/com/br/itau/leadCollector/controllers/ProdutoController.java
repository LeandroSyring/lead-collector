package com.br.itau.leadCollector.controllers;

import com.br.itau.leadCollector.models.Lead;
import com.br.itau.leadCollector.models.Produto;
import com.br.itau.leadCollector.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@RestController
@RequestMapping("/Produtos")
public class ProdutoController {

    @Autowired
    private ProdutoService produtoService;

    @GetMapping
    public Iterable<Produto> buscarTodosProdutos(){
        return produtoService.buscarTodosProdutos();
    }

    @GetMapping("/{id}")
    public Produto buscarProduto(@PathVariable Integer id){

        try {
            return produtoService.buscarProdutoId(id).get();
        }
        catch (Exception Ex){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping
    public ResponseEntity<Produto> inserirProduto(@RequestBody Produto produto){
        Produto produtoReturn = produtoService.salvarProduto(produto);
        return ResponseEntity.status(201).body(produtoReturn);
    }

    @PutMapping("/{id}")
    public Produto atualizarProduto(@RequestBody Produto produto, @PathVariable Integer id){
        produto.setId(id);
        Produto produtoNovo = produtoService.atualizarProduto(produto);
        return produtoNovo;
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Produto> deletarProduto(@PathVariable Integer id){
        Optional<Produto> produto = produtoService.buscarProdutoId(id);

        if(produto.isPresent()){
            produtoService.deletarProduto(produto.get());
            return ResponseEntity.status(204).body(null);
        }
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }
}
