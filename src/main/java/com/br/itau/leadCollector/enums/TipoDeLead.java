package com.br.itau.leadCollector.enums;

public enum TipoDeLead {
    QUENTE,
    ORGANICO,
    FRIO,
}
