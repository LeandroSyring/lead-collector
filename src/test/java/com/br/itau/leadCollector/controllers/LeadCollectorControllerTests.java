package com.br.itau.leadCollector.controllers;

import com.br.itau.leadCollector.enums.TipoDeLead;
import com.br.itau.leadCollector.models.Lead;
import com.br.itau.leadCollector.models.Produto;
import com.br.itau.leadCollector.security.JWTUtil;
import com.br.itau.leadCollector.services.LeadCollectorService;
import com.br.itau.leadCollector.services.UsuarioService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.platform.engine.TestExecutionResult;
import org.mockito.Mockito;
import org.mockito.stubbing.Stubber;
import org.omg.CORBA.Any;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@WebMvcTest(LeadCollectorController.class)
@Import(JWTUtil.class)
public class LeadCollectorControllerTests {

    @MockBean
    LeadCollectorService leadCollectorService;

    @MockBean
    private UsuarioService usuarioService;

    @Autowired
    private MockMvc mockMvc;

    ObjectMapper mapper = new ObjectMapper();

    Lead lead;

    Produto produto;

    @BeforeEach
    public void iniciar() {
        lead = new Lead();
        lead.setNome("Leandro Syring");
        lead.setEmail("leandrosyring@gmail.com");
        lead.setTipoDeLead(TipoDeLead.QUENTE);
        lead.setId(1);

        produto = new Produto();
        produto.setPreco(10.2);
        produto.setId(1);
        produto.setDescricao("Descrição produto");
        produto.setNome("Nome do produto");
        lead.setProdutos(Arrays.asList(produto));
    }

    @Test
    @WithMockUser(username = "usuario@gmail.com", password = "aviao11")
    public void testarInserirLead() throws Exception {

        Iterable produtoIterable = Arrays.asList(produto);

        Mockito.when(leadCollectorService.salvarLead(Mockito.any(Lead.class))).thenReturn(lead);
        Mockito.when(leadCollectorService.buscarTodosProdutos(Mockito.anyList())).thenReturn(produtoIterable);

        String json = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.post("/LeadsCollectors")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(json))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.produtos[0].id", CoreMatchers.equalTo(produto.getId())));
    }

    @Test
    @WithMockUser(username = "usuario@gmail.com", password = "aviao11")
    public void testarBuscaTodosPorId() throws Exception {

        Optional<Lead> leadOptional = Optional.of(lead);

        Mockito.when(leadCollectorService.buscarPorId(Mockito.anyInt())).thenReturn(leadOptional);

        Iterable<Lead> leadIterable = Arrays.asList(lead);

        mockMvc.perform(MockMvcRequestBuilders.get("/LeadsCollectors/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo(lead.getNome())));

    }

    @Test
    @WithMockUser(username = "usuario@gmail.com", password = "aviao11")
    public void testarFalhaBuscaTodosPorId() throws Exception {

        Optional leadOptionalVazio = Optional.empty();

        Mockito.when(leadCollectorService.buscarPorId(Mockito.anyInt())).thenReturn(leadOptionalVazio);

        mockMvc.perform(MockMvcRequestBuilders.get("/LeadsCollectors/1000"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    @WithMockUser(username = "usuario@gmail.com", password = "aviao11")
    public void testarBuscaTotal() throws Exception {

        Iterable<Lead> leadIterable = Arrays.asList(lead);

        Mockito.when(leadCollectorService.buscarTodosLeads()).thenReturn(leadIterable);

        mockMvc.perform(MockMvcRequestBuilders.get("/LeadsCollectors")
                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].nome", CoreMatchers.equalTo(lead.getNome())));
    }

    @Test
    @WithMockUser(username = "usuario@gmail.com", password = "aviao11")
    public void testarAtualizarLead() throws Exception {

        lead.setId(2);

        Mockito.when(leadCollectorService.atualizarLead(Mockito.any(Lead.class))).thenReturn(lead);

        String json = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.put("/LeadsCollectors/1")
            .contentType(MediaType.APPLICATION_JSON)
            .content(json)
            )
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(lead.getId())));

    }

    @Test
    @WithMockUser(username = "usuario@gmail.com", password = "aviao11")
    public void testarDeletarLead() throws Exception {

        Optional<Lead> leadOptional = Optional.of(lead);

        Mockito.when(leadCollectorService.buscarPorId(Mockito.anyInt())).thenReturn(leadOptional);
        leadCollectorService.deletarLead(lead);
        Mockito.verify(leadCollectorService).deletarLead(Mockito.any(Lead.class));

        mockMvc.perform(MockMvcRequestBuilders.delete("/LeadsCollectors/1")
                )
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo(lead.getNome())));
    }

    @Test
    @WithMockUser(username = "usuario@gmail.com", password = "aviao11")
    public void testarDeletarLeadComException() throws Exception {

        Optional<Lead> leadOptionalVazio = Optional.empty();

        Mockito.when(leadCollectorService.buscarPorId(Mockito.anyInt())).thenReturn(leadOptionalVazio);

        mockMvc.perform(MockMvcRequestBuilders.delete("/LeadsCollectors/1000")
        )
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }
}