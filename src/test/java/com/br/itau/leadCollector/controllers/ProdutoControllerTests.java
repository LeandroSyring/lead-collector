package com.br.itau.leadCollector.controllers;

import com.br.itau.leadCollector.models.Lead;
import com.br.itau.leadCollector.models.Produto;
import com.br.itau.leadCollector.security.JWTUtil;
import com.br.itau.leadCollector.services.ProdutoService;
import com.br.itau.leadCollector.services.UsuarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.client.match.MockRestRequestMatchers;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.bind.annotation.Mapping;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@WebMvcTest(ProdutoController.class)
@Import(JWTUtil.class)
public class ProdutoControllerTests {

    @MockBean
    ProdutoService produtoService;

    @MockBean
    private UsuarioService usuarioService;

    @Autowired
    private MockMvc mockMvc;

    ObjectMapper mapper = new ObjectMapper();

    Produto produto;

    @BeforeEach
    public void inicializar(){
        produto = new Produto();
        produto.setDescricao("Descricao Produto");
        produto.setId(1);
        produto.setPreco(18.20);
        produto.setNome("Nome Produto");
    }

    @Test
    @WithMockUser(username = "usuario@gmail.com", password = "aviao11")
    public void testarBuscarTodosProdutos() throws Exception {

        Iterable<Produto> produtoIterable = Arrays.asList(produto);
        Mockito.when(produtoService.buscarTodosProdutos()).thenReturn(produtoIterable);

        mockMvc.perform(MockMvcRequestBuilders.get("/Produtos"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id", CoreMatchers.equalTo(((List<Produto>)produtoIterable).get(0).getId())));
    }

    @Test
    @WithMockUser(username = "usuario@gmail.com", password = "aviao11")
    public void testarBuscarPorId() throws Exception{

        Optional produtoOptional = Optional.of(produto);
        Mockito.when(produtoService.buscarProdutoId(Mockito.anyInt())).thenReturn(produtoOptional);

        mockMvc.perform(MockMvcRequestBuilders.get("/Produtos/2")
                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(produto.getId())));

    }

    @Test
    @WithMockUser(username = "usuario@gmail.com", password = "aviao11")
    public void testarNegativoBuscarPorId() throws Exception{

        Optional produtoOptional = Optional.empty();
        Mockito.when(produtoService.buscarProdutoId(Mockito.anyInt())).thenReturn(produtoOptional);

        mockMvc.perform(MockMvcRequestBuilders.get("/Produtos/3")
                .contentType(MediaType.APPLICATION_JSON)
        )
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    @WithMockUser(username = "usuario@gmail.com", password = "aviao11")
    public void testarInserirProduto() throws Exception {

        Mockito.when(produtoService.salvarProduto(Mockito.any(Produto.class))).thenReturn(produto);

        String json = mapper.writeValueAsString(produto);

        mockMvc.perform(MockMvcRequestBuilders.post("/Produtos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.preco",CoreMatchers.equalTo(produto.getPreco())));
    }

    @Test
    @WithMockUser(username = "usuario@gmail.com", password = "aviao11")
    public void testarAtualizarProduto() throws Exception {

        produto.setDescricao("Atualização Produto");

        Mockito.when(produtoService.atualizarProduto(Mockito.any(Produto.class))).thenReturn(produto);

        String json = mapper.writeValueAsString(produto);

        mockMvc.perform(MockMvcRequestBuilders.put("/Produtos/1")
            .contentType(MediaType.APPLICATION_JSON)
            .content(json))
            .andExpect(MockMvcResultMatchers.jsonPath("$.descricao", CoreMatchers.equalTo(produto.getDescricao())));
    }

    @Test
    @WithMockUser(username = "usuario@gmail.com", password = "aviao11")
    public void testarDeletarProduto() throws Exception {

        Optional<Produto> produtoOptional = Optional.of(produto);

        Mockito.when(produtoService.buscarProdutoId(Mockito.anyInt())).thenReturn(produtoOptional);

        produtoService.deletarProduto(produto);
        Mockito.verify(produtoService).deletarProduto(Mockito.any(Produto.class));

        mockMvc.perform(MockMvcRequestBuilders.delete("/Produtos/1"))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    @WithMockUser(username = "usuario@gmail.com", password = "aviao11")
    public void testarDeletarProdutoInexistente() throws Exception {

        Optional<Produto> produtoOptional = Optional.empty();

        Mockito.when(produtoService.buscarProdutoId(Mockito.anyInt())).thenReturn(produtoOptional);

        produtoService.deletarProduto(produto);
        Mockito.verify(produtoService).deletarProduto(Mockito.any(Produto.class));

        mockMvc.perform(MockMvcRequestBuilders.delete("/Produtos/1"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }


}
