package com.br.itau.leadCollector.services;

import com.br.itau.leadCollector.models.Produto;
import com.br.itau.leadCollector.repositories.ProdutoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class ProdutoServiceTests {

    @MockBean
    ProdutoRepository produtoRepository;

    @Autowired
    ProdutoService produtoService;

    Produto produto;

    @BeforeEach
    public void iniciar(){
        produto = new Produto();
        produto.setDescricao("Descricao Produto");
        produto.setNome("Nome Produto");
        produto.setPreco(10.0);
        produto.setId(1);
    }

    @Test
    public void testarBuscarTodosProdutos(){

        Produto produto2 = new Produto();
        produto2.setDescricao("Descricao Produto 2");
        produto2.setNome("Nome Produto 2");
        produto2.setPreco(20.0);
        produto2.setId(2);

        List<Produto> produtoIterable = new ArrayList<>();
        produtoIterable.add(produto);
        produtoIterable.add(produto2);

        Mockito.when(produtoRepository.findAll()).thenReturn((Iterable<Produto>)produtoIterable);
        Iterable<Produto> produtosIterableBusca = produtoService.buscarTodosProdutos();

        Assertions.assertEquals(produtosIterableBusca, produtoIterable);
    }

    @Test
    public void testarBuscarProdutoPorId(){

        Optional<Produto> produtoOptional = Optional.of(produto);

        Mockito.when(produtoRepository.findById(Mockito.anyInt())).thenReturn(produtoOptional);

        Optional<Produto> produtoObjeto = produtoService.buscarProdutoId(produto.getId());

        Assertions.assertEquals(produtoObjeto, produtoOptional);
    }

    @Test
    public void testarSalvarProduto(){

        Mockito.when(produtoRepository.save(Mockito.any(Produto.class))).thenReturn(produto);

        Produto produtoObjeto = produtoService.salvarProduto(produto);

        Assertions.assertEquals(produtoObjeto, produto);
    }

    @Test
    public void testarAtualizarProduto(){

        Optional<Produto> produtoOptional = Optional.of(produto);

        Mockito.when(produtoRepository.findById(Mockito.anyInt())).thenReturn(produtoOptional);

        produto.setDescricao("Teste Funcionalidade de Atualização");

        Mockito.when(produtoRepository.save(Mockito.any(Produto.class))).thenReturn(produto);

        Produto produtoObjeto = produtoService.atualizarProduto(produto);

        Assertions.assertEquals(produtoObjeto, produto);
    }

    @Test
    public void testarAtualizarProdutoNomeNull(){

        Optional<Produto> produtoOptional = Optional.of(produto);

        Mockito.when(produtoRepository.findById(Mockito.anyInt())).thenReturn(produtoOptional);

        produto.setNome(null);

        produto.setDescricao("Teste Funcionalidade de Atualização");

        Mockito.when(produtoRepository.save(Mockito.any(Produto.class))).thenReturn(produto);

        Produto produtoObjeto = produtoService.atualizarProduto(produto);

        Assertions.assertEquals(produtoObjeto, produto);
    }

    @Test
    public void testarAtualizarProdutoPrecoNull(){

        Optional<Produto> produtoOptional = Optional.of(produto);

        Mockito.when(produtoRepository.findById(Mockito.anyInt())).thenReturn(produtoOptional);

        produto.setPreco(null);

        produto.setDescricao("Teste Funcionalidade de Atualização");

        Mockito.when(produtoRepository.save(Mockito.any(Produto.class))).thenReturn(produto);

        Produto produtoObjeto = produtoService.atualizarProduto(produto);

        Assertions.assertEquals(produtoObjeto, produto);
    }

    @Test
    public void testarAtualizarProdutoDescricaoNull(){

        Optional<Produto> produtoOptional = Optional.of(produto);

        Mockito.when(produtoRepository.findById(Mockito.anyInt())).thenReturn(produtoOptional);

        produto.setDescricao(null);

        produto.setNome("Teste Funcionalidade de Atualização");

        Mockito.when(produtoRepository.save(Mockito.any(Produto.class))).thenReturn(produto);

        Produto produtoObjeto = produtoService.atualizarProduto(produto);

        Assertions.assertEquals(produtoObjeto, produto);
    }

    @Test
    public void testarDeletarProduto(){

        produtoService.deletarProduto(produto);
        Mockito.verify(produtoRepository).delete(Mockito.any(Produto.class));
    }

}