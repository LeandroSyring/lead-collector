package com.br.itau.leadCollector.services;

import com.br.itau.leadCollector.enums.TipoDeLead;
import com.br.itau.leadCollector.models.Lead;
import com.br.itau.leadCollector.models.Produto;
import com.br.itau.leadCollector.repositories.LeadRepository;
import com.br.itau.leadCollector.repositories.ProdutoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class LeadCollectorServiceTests {

    @MockBean
    LeadRepository leadRepository;

    @MockBean
    ProdutoRepository produtoRepository;

    @Autowired
    LeadCollectorService leadCollectorService;

    Lead lead;

    Produto produto;

    @BeforeEach
    public void iniciar(){
        lead = new Lead();
        lead.setId(1);
        lead.setNome("Leandro Syring");
        lead.setEmail("leandrosyring@gmail.com");
        lead.setTipoDeLead(TipoDeLead.QUENTE);
        lead.setProdutos(Arrays.asList(new Produto()));
    }

    @Test
    public void testarSalvarLead(){

        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).thenReturn(lead);

        Lead leadObjeto = leadCollectorService.salvarLead(lead);
        Assertions.assertEquals(leadObjeto.getEmail(), lead.getEmail());
        Assertions.assertEquals(leadObjeto,lead);
    }

    @Test
    public void testeDeletarLead(){
        leadCollectorService.deletarLead(lead);
        Mockito.verify(leadRepository).delete(Mockito.any(Lead.class));
    }

    @Test
    public void testarBuscarTodosProdutos(){

        produto = new Produto();
        produto.setDescricao("Descricao Produto");
        produto.setNome("Nome Produto");
        produto.setPreco(10.0);
        produto.setId(1);

        Produto produto2 = new Produto();
        produto2.setDescricao("Descricao Produto 2");
        produto2.setNome("Nome Produto 2");
        produto2.setPreco(20.0);
        produto2.setId(2);

        List<Produto> produtoIterable = new ArrayList<>();
        produtoIterable.add(produto);
        produtoIterable.add(produto2);


        List<Integer> idsProdutos = new ArrayList();
        idsProdutos.add(1);
        idsProdutos.add(2);

        Mockito.when(produtoRepository.findAllById(Mockito.anyList())).thenReturn((Iterable<Produto>)produtoIterable);
        Iterable<Produto> produtosIterableBusca = leadCollectorService.buscarTodosProdutos(idsProdutos);

        Assertions.assertEquals(produtosIterableBusca, produtoIterable);

    }

    @Test
    public void testarBuscarPorID(){

        Optional<Lead> leadOptional = Optional.of(lead);

        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(leadOptional);

        Optional<Lead> leadBusca = leadCollectorService.buscarPorId(lead.getId());

        Assertions.assertEquals(leadBusca, leadOptional);
    }

    @Test
    public void testarBuscarTodosLeads(){

        Lead lead2 = new Lead();
        lead2.setId(2);
        lead2.setTipoDeLead(TipoDeLead.QUENTE);
        lead2.setEmail("teste@gmail.com");
        lead2.setNome("Leandro Syring");
        lead2.setProdutos(Arrays.asList(new Produto()));

        List<Lead> leadList = new ArrayList<>();
        leadList.add(lead);
        leadList.add(lead2);

        Mockito.when(leadRepository.findAll()).thenReturn((Iterable<Lead>)leadList);

        Iterable<Lead> leadsBusca = leadCollectorService.buscarTodosLeads();

        Assertions.assertEquals(leadsBusca, leadList);
    }

    @Test
    public void testarAtualizarLead(){

        Optional<Lead> leadOptional = Optional.of(lead);

        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(leadOptional);

        lead.setNome("teste atualizacao");

        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).thenReturn(lead);

        Lead leadObjeto = leadCollectorService.atualizarLead(lead);

        Assertions.assertEquals(lead.getNome(),leadObjeto.getNome());
    }

    @Test
    public void testarAtualizarLeadEmailNull(){

        Optional<Lead> leadOptional = Optional.of(lead);

        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(leadOptional);

        lead.setNome("teste atualizacao");
        lead.setEmail(null);

        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).thenReturn(lead);

        Lead leadObjeto = leadCollectorService.atualizarLead(lead);

        Assertions.assertEquals(lead.getNome(),leadObjeto.getNome());
    }

    @Test
    public void testarAtualizarLeadTipoLeadNull(){

        Optional<Lead> leadOptional = Optional.of(lead);

        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(leadOptional);

        lead.setNome("teste atualizacao");
        lead.setTipoDeLead(null);

        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).thenReturn(lead);

        Lead leadObjeto = leadCollectorService.atualizarLead(lead);

        Assertions.assertEquals(lead.getNome(),leadObjeto.getNome());
    }

    @Test
    public void testarAtualizarLeadNomeNull(){

        Optional<Lead> leadOptional = Optional.of(lead);

        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(leadOptional);

        lead.setEmail("testetdd@gmail.com");
        lead.setNome(null);

        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).thenReturn(lead);

        Lead leadObjeto = leadCollectorService.atualizarLead(lead);

        Assertions.assertEquals(lead.getEmail(),leadObjeto.getEmail());
    }

}
